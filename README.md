# ElasticSearch_BigData_ESGI



## Installation


* Cloner le projet `git clone git@gitlab.com:yaniselmaleh/elasticsearch_bigdata_esgi.git`
* Se rendre dans le dossier `cd elasticsearch_bigdata_esgi`
* Build le docker `docker-compose build ou make build`
* Lancer le projet `docker-compose up -d ou make start`

```bash
http://localhost:5601

http://localhost:9600/
```

* Pour lancer les requetes du TP `make nom_exo`  Ex: `make ex2`
