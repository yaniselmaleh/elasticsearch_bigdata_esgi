build:
	docker-compose build
	docker-compose run --rm node npm install @elastic/elasticsearch

start:
	docker-compose up -d

stop:
	docker-compose down

import:
	docker-compose run --rm node node import-data.js

curl:
	curl --user elastic:changeme -X PUT "localhost:9200/_groupe_5/_bulk" -H 'Content-Type: application/json' --data-binary @data.json

ex2:
	node tp/ex9.js

ex3:
	node tp/ex3.js

ex4:
	node tp/ex4.js

ex5:
	node tp/ex5.js

ex6:
	node tp/ex6.js

ex7:
	node tp/ex7.js

ex8:
	node tp/ex8.js

ex9:
	node tp/ex9.js

ex10:
	node tp/ex10.js

ex11:
	node tp/ex11.js


ex12:
	node tp/ex12.js

