
function importCsvCustom() {

    let fs = require ('fs');
    let filePath = "./data/data.csv";

    // get doc from
    let doc = fs.readFileSync(filePath).toString().split('\n');

    // prepare final string
    let json = '';

    // row to insert for Bulk
    let indexRef = {"index" : {"_index": "groupe_5", "_type":"_doc"}};

    // parse data
    for (let i = 0; i < doc.length; i++) {

        // replace " , " separator by " ; "
        let row = doc[i].replace(/","/gi,'";"');

        // get words from same line
        let words = row.split(";");

        let pivot = [];

        // this part is to try clear data,
        try {
            pivot = words.map( (e) => JSON.parse(e));
        } catch (e) {
            pivot = words;
        }

        // create object with actual row parsed
        let currentObj = {
            title: pivot[0] ,
            subTitle: pivot[1] ,
            link: pivot[2] ,
            author: pivot[3] ,
            date: pivot[4] ,
            category: pivot[5] ,
            languages: pivot[6] ,
            content: pivot[7]
        };

        // add content to final string
        json += JSON.stringify(indexRef) + "\n";
        json += JSON.stringify(currentObj) + "\n";

    }
    // create file
    fs.writeFile('data.json', json, () => {
        console.log('ok')
    });
}
importCsvCustom();
