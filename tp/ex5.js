const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://elastic:changeme@localhost:9200' })

const result = client.search({
    index: 'groupe_5',
    body: {
        query: {
            bool: {
                must: [
                    {
                        match: {
                            title: "elastic"
                        }
                    }
                ]
            }
        }
    }
}).then( response => {
    // console.log(response.body);
    console.log(response.body);
})