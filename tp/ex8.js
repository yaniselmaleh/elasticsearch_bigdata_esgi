const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://elastic:changeme@localhost:9200' })

const result = client.search({
    index: 'groupe_5',
    body: {
        query: {
            bool: {
                should: [
                    {
                        match: {
                            content: "search"
                        }
                    }
                ]
            }
        }
    }
}).then( response => {
    // console.log(response.body);
    console.log(response.body);
})


const result2 = client.search({
    index: 'groupe_5',
    body: {
        query: {
            bool: {
                should: [
                    {
                        match: {
                            content: "search"
                        }
                    },
                    {
                        match: {
                            content: "analytics"
                        }
                    }
                ]
            }
        }
    }
}).then( response => {
    // console.log(response.body);
    console.log(response.body);
})

const result3 = client.search({
    index: 'groupe_5',
    body: {
        query: {
            bool: {
                should: [
                    {
                        match_phrase: {
                            content: "search analytics"
                        }
                    },
                ]
            }
        }
    }
}).then( response => {
    // console.log(response.body);
    console.log(response.body);
})