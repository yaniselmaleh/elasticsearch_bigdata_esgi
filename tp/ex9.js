const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://elastic:changeme@localhost:9200' })

const result = client.search({
    index: 'groupe_5',
    body: {
        query: {
            bool: {
                must: [
                    {
                        match_phrase: {
                            content: "search analytics"
                        }
                    }
                ]
            }
        },
        aggs: {
            top_content: {
                top_hits: {
                    size: 3
                }
            }
        },
        size: 3
    }
}).then( response => {
    console.log(response.body);
})