const { Client } = require('@elastic/elasticsearch')
const client = new Client({ node: 'http://elastic:changeme@localhost:9200' })

const result = client.search({
    index: 'groupe_5',
    body: {
        query: {
            range: {
                date: {
                    gte: "2017-05-01",
                    lt: "2017-05-01"
                }
            }
        }
    }
}).then( response => {
    // console.log(response.body);
    console.log(response.body);
})